package com.bodega.owner.fragment;

import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.model.ShopListModel;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Fragment_map_short extends Fragment implements OnMapReadyCallback {

	private GoogleMap mMap;
	SupportMapFragment mapFragment;
	LocationManager locationManager;

	String provider;

	ImageButton profile;
	TextView topTxt;
	Marker m1;

	int success = 0;

	RelativeLayout tutorial;
	Location location;

	JSONParser jsonParser;

	ArrayList<Marker> markers;

	JSONObject json;

	String msg, message;




	String shopContact, shopID;

	Typeface mytTypeface, customFont;

	SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

	public ArrayList<ShopListModel> shopList;
	ShopListModel shopListModel;

	Map<String, String> shopMap = new HashMap<String, String>();



	TextView titleText;

	double lat, lon;

	String firstTime;
	LatLng firstPoint = null;
	LatLng secondPoint = null;

	float distanceInMeter;

	LatLng currentLoc;
	TextView distanceInfo;
	ArrayList<LatLng> trackList;
	double Lat, Lon;

	String loginStatus;

	Button gotIt;
	SharePref sharePref;

	ConnectionDetector cd;

	boolean isInternetON;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_maps, container, false);


		Bundle bundle = getArguments();

		lat = bundle.getDouble("lat");

		lon = bundle.getDouble("lon");



		cd  = new ConnectionDetector(getActivity());

		isInternetON = cd.isConnectingToInternet();


		mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

		//mMap = mapFragment.getMap();

		mapFragment.getMapAsync(this);





		sharePref = new SharePref(getActivity());


		markers = new ArrayList<>();
		shopList = new ArrayList<>();

		jsonParser = new JSONParser();

		//providerCheck();


		return rootView;
	}

//	public void providerCheck(){
//
//
//		if(isInternetON) {
//			setUpMap();
//
//		}else{
//			Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
//
//		}
//
//
//	}


	public void setUpMap() {




		mMap.setMyLocationEnabled(true);

		MarkerOptions markerOptions = new MarkerOptions();




		mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				return false;
			}
		});



		setLocation(lat, lon);



	}




	public void setLocation(double lat, double lon) {

      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 15.0f));
 	 	m1 = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
	 	m1.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_shop));

	}


	@Override
	public void onMapReady(GoogleMap mMap) {
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 15.0f));
		m1 = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
		m1.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_shop));
	}
}