package com.bodega.owner.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;


public class Fragment_profile_test extends Fragment {

	TextView generalInfo, storeInfo,images;

	View first, second;

	Boolean isInternetPresent = false;


	// Connection detector class
	ConnectionDetector cd;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		View view = inflater.inflate(R.layout.fragment_profile, container,
				false);

		// getActionBar().setDisplayHomeAsUpEnabled(true);
		// getActionBar().setHomeButtonEnabled(true);

		cd = new ConnectionDetector(getActivity());

		isInternetPresent = cd.isConnectingToInternet();


		generalInfo = (TextView) view.findViewById(R.id.general);
		storeInfo = (TextView) view.findViewById(R.id.storeProfile);

		images = (TextView) view.findViewById(R.id.images);


		first = (View) view.findViewById(R.id.first);
		second = (View) view.findViewById(R.id.second);

		displayViewFirstPage(0);

		generalInfo.setBackgroundColor(getResources().getColor(R.color.bodega2));

		generalInfo.setTextColor(getResources().getColor(R.color.white_text));

		images.setBackgroundColor(getResources().getColor(R.color.black_overlay2));

		images.setTextColor(getResources().getColor(R.color.bodega2));

		storeInfo.setBackgroundColor(getResources().getColor(R.color.black_overlay2));

		storeInfo.setTextColor(getResources().getColor(R.color.bodega2));

		generalInfo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				displayViewFirstPage(0);

				generalInfo.setBackgroundColor(getResources().getColor(R.color.bodega2));

				generalInfo.setTextColor(getResources().getColor(R.color.white_text));

				images.setBackgroundColor(getResources().getColor(R.color.black_overlay2));

				images.setTextColor(getResources().getColor(R.color.bodega2));

				storeInfo.setBackgroundColor(getResources().getColor(R.color.black_overlay2));

				storeInfo.setTextColor(getResources().getColor(R.color.bodega2));

			}
		});


		images.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				displayViewFirstPage(2);

				images.setBackgroundColor(getResources().getColor(R.color.bodega2));

				images.setTextColor(getResources().getColor(R.color.white_text));

				generalInfo.setBackgroundColor(getResources().getColor(R.color.black_overlay2));

				generalInfo.setTextColor(getResources().getColor(R.color.bodega2));

				storeInfo.setBackgroundColor(getResources().getColor(R.color.black_overlay2));

				storeInfo.setTextColor(getResources().getColor(R.color.bodega2));
			}
		});


		storeInfo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				displayViewFirstPage(1);

				storeInfo.setBackgroundColor(getResources().getColor(R.color.bodega2));

				storeInfo.setTextColor(getResources().getColor(R.color.white_text));

				images.setBackgroundColor(getResources().getColor(R.color.black_overlay2));

				images.setTextColor(getResources().getColor(R.color.bodega2));

				generalInfo.setBackgroundColor(getResources().getColor(R.color.black_overlay2));

				generalInfo.setTextColor(getResources().getColor(R.color.bodega2));


			}
		});





		if (isInternetPresent) {



		} else {
			Toast.makeText(getActivity(), "No internet connection available",
					Toast.LENGTH_LONG).show();
		}

		return view;
	}

	public class ConnectionDetector {

		private Context _context;

		public ConnectionDetector(Context context) {
			this._context = context;
		}

		public boolean isConnectingToInternet() {
			ConnectivityManager connectivity = (ConnectivityManager) _context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null)
					for (int i = 0; i < info.length; i++)
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							return true;
						}

			}
			return false;
		}
	}

//	@Override
//	public void onActivityCreated(Bundle savedInstanceState)
//	{
//		super.onActivityCreated(savedInstanceState);
//
//		FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//		fontChanger.replaceFonts((ViewGroup) this.getView());
//	}

	private void displayViewFirstPage(int position) {
		// update the main content by replacing fragments

		Fragment fragment = null;

		switch (position) {
			case 0:
				fragment = new GeneralInfo();

				break;
			case 1:
				fragment = new OtherInfo();


				break;
			case 2:
				fragment = new PictureFragment();


				break;

			default:
				break;
		}

		if (fragment != null) {

			FragmentManager mFragmentManager = getChildFragmentManager();

			FragmentTransaction ft = mFragmentManager.beginTransaction();
//            Bundle args = new Bundle();
//            args.putString("token", token);
//
//            fragment.setArguments(args);

			ft.replace(R.id.sub_fragment_container, fragment);

			ft.commit();

		}
	}

}