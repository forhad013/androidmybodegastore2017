package com.bodega.owner.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.model.DeliveryOption;
import com.bodega.owner.model.JsonModelDelivery;
import com.bodega.owner.model.JsonModelHours;
import com.bodega.owner.model.JsonModelImages;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Nahid on 1/18/2016.
 */
public class OtherInfo extends Fragment {


    String errorString;
    ConnectionDetector cd;
    int success = 0;
    JSONObject json;
    String msg;
    boolean isInternetOn = false;
    ArrayList timeList, ampmlist;
    ArrayList hours, additaionalImagePath;

    JSONParser jsonParser;
    String userID;
    Button update;

    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;
    boolean d0,d1, d2, d3, d4;

    String d2value, d3value, d4value;

    TextView time11, time12, time21, time22, time31, time32, time41, time42, time51, time52, time61, time62, time71, time72;


    ArrayList<DeliveryOption> deliveryOptionArrayList;
    DeliveryOption deliveryOption;

    EditText dOptionValue2, dOptionValue3, dOptionValue4;
    ImageButton edit, cancel;
    CheckBox checkBox0,checkBox1, checkBox2, checkBox3, checkBox4;
    ArrayList<Integer> timeListInt;
    String dayString1, dayString2, dayString3, dayString4, dayString5, dayString6, dayString7;
    EditText legalName;
    SharePref sharePref;
    ArrayAdapter<String> mondayAdapter1, mondayAdapter2, mondayAdapter3, mondayAdapter4, tuesdayAdapter1, tuesdayAdapter2, wednesdayAdapter1, wednesdayAdapter2, thusdayAdapter1, thusdayAdapter2, fridayAdapter1, fridayAdapter2, saturdayAdapter1, sundayAdapter1, saturdayAdapter2, sundayAdapter2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store_info, container, false);
        sharePref = new SharePref(getActivity());

        edit = (ImageButton)view.findViewById(R.id.editStore);
        cancel = (ImageButton)view.findViewById(R.id.cancelStore);

        checkBox0 = (CheckBox) view.findViewById(R.id.deliveryOption0);

        checkBox1 = (CheckBox) view.findViewById(R.id.deliveryOption1);

        checkBox2 = (CheckBox) view.findViewById(R.id.deliveryOption2);

        checkBox3 = (CheckBox) view.findViewById(R.id.deliveryOption3);

        checkBox4 = (CheckBox) view.findViewById(R.id.deliveryOption4);

        dOptionValue2 = (EditText) view.findViewById(R.id.dOptionValue2);

        dOptionValue3 = (EditText) view.findViewById(R.id.dOptionValue3);
        dOptionValue4 = (EditText) view.findViewById(R.id.dOptionValue4);

        cd = new ConnectionDetector(getActivity());
        deliveryOptionArrayList = new ArrayList<>();


        isInternetOn = cd.isConnectingToInternet();

        time11 = (TextView) view.findViewById(R.id.spinnerAM1);
        time12 = (TextView) view.findViewById(R.id.spinnerPM1);

        time21 = (TextView) view.findViewById(R.id.spinnerAM2);
        time22 = (TextView) view.findViewById(R.id.spinnerPM2);

        time31 = (TextView) view.findViewById(R.id.spinnerAM3);
        time32 = (TextView) view.findViewById(R.id.spinnerPM3);

        time41 = (TextView) view.findViewById(R.id.spinnerAM4);
        time42 = (TextView) view.findViewById(R.id.spinnerPM4);

        time51 = (TextView) view.findViewById(R.id.spinnerAM5);
        time52 = (TextView) view.findViewById(R.id.spinnerPM5);

        time61 = (TextView) view.findViewById(R.id.spinnerAM6);
        time62 = (TextView) view.findViewById(R.id.spinnerPM6);

        time71 = (TextView) view.findViewById(R.id.spinnerAM7);
        time72 = (TextView) view.findViewById(R.id.spinnerPM7);


        userID = sharePref.getshareprefdatastring(SharePref.USERID);

        timeListInt = new ArrayList<>();

        update = (Button) view.findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetOn){
                    setHours();
                    setDeliveryOption();

                    new AsyncTaskRunnerDetails().execute();
                }

            }
        });

        ampmlist = new ArrayList();

        timeList = new ArrayList();

        hours = new ArrayList();




        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        d0 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION0);
        d1 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION1);
        d2 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION2);
        d3 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION3);
        d4 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION4);

        d2value = sharePref.getshareprefdatastring(SharePref.D_OPTION2_VALUE);
        d3value = sharePref.getshareprefdatastring(SharePref.D_OPTION3_VALUE);
        d4value = sharePref.getshareprefdatastring(SharePref.D_OPTION4_VALUE);

        dayString1 = sharePref.getshareprefdatastring(SharePref.MONDAY);
        dayString2 = sharePref.getshareprefdatastring(SharePref.TUESDAY);
        dayString3 = sharePref.getshareprefdatastring(SharePref.WEDNESSDAY);
        dayString4 = sharePref.getshareprefdatastring(SharePref.THUSDAY);
        dayString5 = sharePref.getshareprefdatastring(SharePref.FRIDAY);
        dayString6 = sharePref.getshareprefdatastring(SharePref.SATURDAY);
        dayString7 = sharePref.getshareprefdatastring(SharePref.SUNDAY);
        Log.e("day", dayString1 + " & " + dayString2 + "&" + dayString3);





        update.setVisibility(View.INVISIBLE);





        setData();


        setCheckBox();


        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    checkBox2.setChecked(false);
                    checkBox3.setChecked(false);
                    checkBox0.setChecked(false);

                    checkBox2.setEnabled(false);
                    checkBox3.setEnabled(false);


                } else {
                    checkBox2.setEnabled(true);
                    checkBox3.setEnabled(true);
                    checkBox4.setEnabled(true);
                }

                checkEditbox();
            }
        });

        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    checkBox1.setChecked(false);

                    checkBox0.setChecked(false);
                    checkBox1.setEnabled(false);

                    checkBox3.setChecked(false);


                    checkBox3.setEnabled(false);


                } else {



                    checkBox1.setEnabled(true);
                    checkBox3.setEnabled(true);

                }

                checkEditbox();
            }
        });

        checkBox0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkBox1.setChecked(false);

                    checkBox1.setEnabled(false);
                    checkBox2.setChecked(false);

                    checkBox2.setEnabled(false);
                    checkBox3.setChecked(false);

                    checkBox3.setEnabled(false);
                    checkBox4.setChecked(false);

                    checkBox4.setEnabled(false);

                } else {



                    checkBox1.setEnabled(true);
                    checkBox2.setEnabled(true);
                    checkBox3.setEnabled(true);
                    checkBox4.setEnabled(true);

                }

                checkEditbox();
            }
        });

        checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {



                    checkBox1.setChecked(false);


                    checkBox1.setEnabled(false);

                    checkBox2.setChecked(false);
                    checkBox0.setChecked(false);

                    checkBox2.setEnabled(false);

                } else {

                    checkBox2.setEnabled(true);
                    checkBox1.setEnabled(true);


                }

                checkEditbox();

            }
        });






        cancel.setVisibility(View.GONE);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                setEnableOrDisable(true);
                cancel.setVisibility(View.VISIBLE);
                update.setVisibility(View.VISIBLE);

                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                doneDialog.setTitleText("EDIT NOW");
                doneDialog.setContentText("You can now edit your information");
                doneDialog.show();
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                        doneDialog.dismiss();
                    }
                });

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setEnableOrDisable(false);
                cancel.setVisibility(View.GONE);
                update.setVisibility(View.GONE);
                setData();
            }
        });


        time11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time11);
            }
        });
        time12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time12);
            }
        });
        time21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time21);
            }
        });
        time22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time22);
            }
        });
        time31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time31);
            }
        });
        time32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time32);
            }
        });
        time41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time41);
            }
        });

        time42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time42);
            }
        });

        time51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time51);
            }
        });

        time52.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time52);
            }
        });

        time61.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time61);
            }
        });
        time62.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time62);
            }
        });
        time71.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time71);
            }
        });
        time72.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time72);
            }
        });

        setEnableOrDisable(false);
        setData();
        return view;
    }


    public void setData() {

        checkBox0.setChecked(d0);
        checkBox1.setChecked(d1);
        checkBox2.setChecked(d2);
        checkBox3.setChecked(d3);
        checkBox4.setChecked(d4);


        if (d2) {

            dOptionValue2.setText(d2value);
        }

        if (d3) {

            dOptionValue3.setText(d3value);
        }

        if (d4) {

            dOptionValue4.setText(d4value);
        }
        setTimeList();

    }



//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }

    public void setCheckBox(){

        if (checkBox3.isChecked()) {


            checkBox2.setChecked(false);


            checkBox2.setEnabled(false);


        } else {

            checkBox2.setEnabled(true);

        }


        if (checkBox0.isChecked()) {

            checkBox1.setChecked(false);

            checkBox1.setEnabled(false);
            checkBox2.setChecked(false);

            checkBox2.setEnabled(false);
            checkBox3.setChecked(false);

            checkBox3.setEnabled(false);
            checkBox4.setChecked(false);

            checkBox4.setEnabled(false);

        } else {



            checkBox1.setEnabled(true);


            checkBox2.setEnabled(true);


            checkBox3.setEnabled(true);


            checkBox4.setEnabled(true);

        }



        if (checkBox1.isChecked()) {

            checkBox2.setChecked(false);
            checkBox3.setChecked(false);


            checkBox2.setEnabled(false);
            checkBox3.setEnabled(false);


        } else {
            checkBox2.setEnabled(true);
            checkBox3.setEnabled(true);
            checkBox4.setEnabled(true);
        }

        if (checkBox2.isChecked()) {

            checkBox1.setChecked(false);


            checkBox1.setEnabled(false);

            checkBox3.setChecked(false);


            checkBox3.setEnabled(false);
        }
        checkEditbox();
    }

    public void checkEditbox(){

        dOptionValue2.setEnabled(false);
        dOptionValue3.setEnabled(false);
        dOptionValue4.setEnabled(false);

        if(checkBox0.isChecked()){
            dOptionValue2.setEnabled(false);
            dOptionValue3.setEnabled(false);
            dOptionValue4.setEnabled(false);
        }

        if(checkBox1.isChecked()){


        }

        if(checkBox2.isChecked()){
            dOptionValue2.setEnabled(true);


        }
        if(checkBox3.isChecked()){
            dOptionValue3.setEnabled(true);


        }

    }

    public int getTime(String data) {

        int position = 0;

        switch (data) {
            case "00.00":

                position = 0;
                break;
            case "01.00":

                position = 1;
                break;
            case "02.00":
                position = 2;
                break;

            case "03.00":
                position = 3;

                break;
            case "04.00":
                position = 4;

                break;

            case "05.00":
                position = 5;
                break;
            case "06.00":
                position = 6;
                break;

            case "07.00":
                position = 7;

                break;
            case "08.00":
                position = 8;

                break;
            case "09.00":
                position = 9;
                break;
            case "10.00":
                position = 10;
                break;
            case "11.00":
                position = 11;
                break;

            case "12.00":
                position = 12;

                break;
            default:
                break;



        }
        return position;
    }

    public int getAMPM(String data) {

        int position = 0;

        switch (data) {
            case "am":

                position = 0;
                break;
            case "pm":

                position = 1;
                break;

            default:
                break;



        }
        return position;
    }


    public void setEnableOrDisable(boolean value){

        checkBox0.setEnabled(value);
        checkBox1.setEnabled(value);
        checkBox2.setEnabled(value);
        checkBox3.setEnabled(value);
        checkBox4.setEnabled(value);

        dOptionValue2.setEnabled(value);
        dOptionValue3.setEnabled(value);
        dOptionValue4.setEnabled(value);


        time11.setEnabled(value);
        time12 .setEnabled(value);
        time21.setEnabled(value);
        time22.setEnabled(value);

        time31.setEnabled(value);
        time32.setEnabled(value);

        time41.setEnabled(value);
        time42.setEnabled(value);

        time51.setEnabled(value);
        time52.setEnabled(value);

        time61.setEnabled(value);
        time62.setEnabled(value);

        time71.setEnabled(value);
        time72.setEnabled(value);




      //  setCheckBox();

    }


    class AsyncTaskRunnerDetails extends AsyncTask<String, String, String> {

        int success = 5, mailcheck = 5;

        HttpResponse response;
        JSONObject response1;
        String responseBody;
        Boolean availableProduct, availablemail;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            {

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        Url.UPDATE);

                httpPost.setHeader("content-type", "application/json");
                httpPost.setHeader("Accept", "application/json");
                //  JSONObject jsonObject1 = new JSONObject();
                JSONObject jsonObject = new JSONObject();


                ArrayList<JsonModelDelivery> jsonDelivery = new ArrayList<>();

                ArrayList<JsonModelImages> jsonImagesList = new ArrayList<>();
                ArrayList<JsonModelHours> jsonHours = new ArrayList<>();

           //     setDeliveryOption();





                try{



                    for(int i=0;i<5;i++){
                        JsonModelDelivery delivery = new JsonModelDelivery();
                        delivery.optionName = deliveryOptionArrayList.get(i).optionName;
                        delivery.value  = deliveryOptionArrayList.get(i).value+"";

                        delivery.amount  = deliveryOptionArrayList.get(i).amount;
                        jsonDelivery.add(delivery);


                    }


                    JSONArray jsonArray = new JSONArray();
                    for (int i=0;i < jsonDelivery.size();i++){
                        jsonArray.put(jsonDelivery.get(i).getJSONObject());
                    }

                    for(int i=0;i<7;i++){
                        JsonModelHours jsonModelHours = new JsonModelHours();
                        jsonModelHours.time = hours.get(i).toString();

                        jsonHours.add(jsonModelHours);


                        //Log.e("su",json_submodel.PID[0]+"");

                    }




                    JSONArray jsonArrayHours = new JSONArray();
                    for (int i=0;i < 7;i++){
                        jsonArrayHours.put(jsonHours.get(i).getJSONObject());
                    }





                    jsonObject.accumulate("storeUpdateType", "storeInfo");

                    jsonObject.accumulate("userId", userID);


                    jsonObject.accumulate("userHours",jsonArrayHours);

                    jsonObject.accumulate("userDeliveryFees",jsonArray);
                    jsonObject.accumulate("userType","1");








                    Log.e("res",jsonObject.toString()+"");

                }
                catch (Exception e) {
//
//                    Log.d("InputStream", e.getLocalizedMessage());

                }

                StringEntity entity = null;




                try {
                    entity = new StringEntity( jsonObject.toString());
                    entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    // Log.d("ch",entity+"");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                httpPost.setEntity(entity);

                try {
                    //  response = httpClient.execute(httpPost);
//                    resC++;
//                    Log.e("Value of response count: ", resC + "");
                    ResponseHandler<String> responseHandler=new BasicResponseHandler();
                    responseBody =httpClient.execute(httpPost, responseHandler);
                    response1=new JSONObject(responseBody);
                    Log.e("res", responseBody + "");





                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(String string) {

            progressSweetAlertDialog.dismiss();

            int success;
            String msg = "";
            try {
                success =  response1.getInt("success");

                msg = response1.getString("message");

                if(success==1){


                    doneDialog=  new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("Success");
                    doneDialog.setContentText(msg);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            doneDialog.dismiss();

                            setEnableOrDisable(false);
                            update.setVisibility(View.INVISIBLE);
                            setPref();
                        }
                    });


                }else if(success ==0){

                    getError(response1);



                    doneDialog=  new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);


                    doneDialog.setContentText(errorString);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    });

                }



            }catch (JSONException e) {
                e.printStackTrace();
            }


            update.setEnabled(true);



        }



        @Override
        protected void onPreExecute() {

            progressSweetAlertDialog.show();
            update.setEnabled(false);

        }

    }

    public void setHours(){

        hours.add(time11.getText().toString().replace(" ","|")  + " - " + time12.getText().toString().replace(" ","|"));
        hours.add(time21.getText().toString().replace(" ","|") +   " - " + time22.getText().toString().replace(" ","|"));
        hours.add(time31.getText().toString().replace(" ","|") +  " - " + time32.getText().toString().replace(" ","|"));
        hours.add(time41.getText().toString().replace(" ","|") +   " - " + time42.getText().toString().replace(" ","|"));
        hours.add(time51.getText().toString().replace(" ","|") +   " - " +time52.getText().toString().replace(" ","|"));
        hours.add(time61.getText().toString().replace(" ","|") +   " - " +time62.getText().toString().replace(" ","|"));
        hours.add(time71.getText().toString().replace(" ","|") +   " - " +time72.getText().toString().replace(" ","|"));

        for(int i =0;i<7;i++){


            Log.e("time"+i,hours.get(i).toString());
        }

    }


    public void getError(JSONObject json){

        String newline = System.getProperty("line.separator");

        try {
            JSONObject result = json.getJSONObject("results");


            JSONArray errors= result.getJSONArray("errors");

            for (int i=0;i<errors.length();i++){

                JSONObject temp = errors.getJSONObject(i);

                errorString = errorString + "* " +  temp.getString("errorMessage") +newline;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setDeliveryOption(){

        deliveryOptionArrayList.clear();

        d0 = checkBox0.isChecked();
        d1 = checkBox1.isChecked();
        d2 = checkBox2.isChecked();
        d3 = checkBox3.isChecked();
        d4 = checkBox4.isChecked();

        if(checkBox2.isChecked()){
            d2value = dOptionValue2.getText().toString();

        }else{
            d2value = "00";
            dOptionValue2.setText("");

        }
        if(checkBox3.isChecked()){
            d3value = dOptionValue3.getText().toString();
        }else{
            d3value = "00";
            dOptionValue3.setText("");

        }
        if(checkBox4.isChecked()){
            d4value = dOptionValue4.getText().toString();
        }else{
            d4value = "00";
            dOptionValue4.setText("");
        }


        deliveryOption = new DeliveryOption("d0",d0,"0");
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d1",d1,"0");
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d2",d2,d2value);
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d3",d3,d3value);
        deliveryOptionArrayList.add(deliveryOption);
        deliveryOption = new DeliveryOption("d4",d4, d4value);
        deliveryOptionArrayList.add(deliveryOption);


    }


    public void setPref(){

        sharePref.setshareprefdataBoolean(SharePref.D_OPTION0,d0);
        sharePref.setshareprefdataBoolean(SharePref.D_OPTION1,d1);
        sharePref.setshareprefdataBoolean(SharePref.D_OPTION2,d2);

        sharePref.setshareprefdataBoolean(SharePref.D_OPTION3,d3);
        sharePref.setshareprefdataBoolean(SharePref.D_OPTION4,d4);

        sharePref.setshareprefdatastring(SharePref.D_OPTION2_VALUE, d2value);
        sharePref.setshareprefdatastring(SharePref.D_OPTION3_VALUE, d3value);
        sharePref.setshareprefdatastring(SharePref.D_OPTION4_VALUE,d4value);

        sharePref.setshareprefdatastring(SharePref.MONDAY,hours.get(0).toString());
        sharePref.setshareprefdatastring(SharePref.TUESDAY,hours.get(1).toString());
        sharePref.setshareprefdatastring(SharePref.WEDNESSDAY,hours.get(2).toString());
        sharePref.setshareprefdatastring(SharePref.THUSDAY,hours.get(3).toString());
        sharePref.setshareprefdatastring(SharePref.FRIDAY,hours.get(4).toString());
        sharePref.setshareprefdatastring(SharePref.SATURDAY,hours.get(5).toString());
        sharePref.setshareprefdatastring(SharePref.SUNDAY,hours.get(6).toString());


        Log.e("dataCheck", hours.get(0).toString());
    }




    public void timePickerDialoge(final TextView tview){
        final Dialog dialog = new Dialog(getActivity());

        final String[] timeString = {""};
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.picker_dialog);
        // Set dialog title
        //dialog.setTitle("Category Add");

        dialog.show();

        Button done = (Button) dialog.findViewById(R.id.done);


        final WheelView wheelView = (WheelView) dialog.findViewById(R.id.wheelview);

        final WheelView amWheelView = (WheelView) dialog.findViewById(R.id.am_wheelview);
        ArrayList timeList = new ArrayList();

        timeList.add("00.00");

        timeList.add("00.30");
        timeList.add("01.00");
        timeList.add("01.30");
        timeList.add("02.00");
        timeList.add("02.30");
        timeList.add("03.00");
        timeList.add("03.30");
        timeList.add("04.00");
        timeList.add("04.30");
        timeList.add("05.00");
        timeList.add("05.30");
        timeList.add("06.00");
        timeList.add("06.30");
        timeList.add("07.00");
        timeList.add("07.30");
        timeList.add("08.00");
        timeList.add("08.30");
        timeList.add("09.00");
        timeList.add("09.30");
        timeList.add("10.00");
        timeList.add("10.30");
        timeList.add("11.00");
        timeList.add("11.30");
        timeList.add("12.00");



        ArrayList ampmList = new ArrayList();

        ampmList.add("am");
        ampmList.add("pm");


        wheelView.setWheelAdapter(new ArrayWheelAdapter(getActivity()));
        wheelView.setSkin(WheelView.Skin.Holo);
        wheelView.setWheelData(timeList);

        amWheelView.setWheelAdapter(new ArrayWheelAdapter(getActivity()));
        amWheelView.setSkin(WheelView.Skin.Holo);
        amWheelView.setWheelData(ampmList);
        wheelView.setSelection(10);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                timeString[0] = wheelView.getSelectionItem().toString();
                timeString[0] = timeString[0] +" "+ amWheelView.getSelectionItem().toString();
                Log.e("time",timeString[0]);


                tview.setText(timeString[0]);
                dialog.dismiss();



            }
        });


        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                timeString[0] = wheelView.getSelectionItem().toString();
                timeString[0] = timeString[0] +" "+ amWheelView.getSelectionItem().toString();
                Log.e("time",timeString[0]);


                tview.setText(timeString[0]);
            }
        });

    }

    public void setTimeList(){










        String[] monday = dayString1.split("\\ - ");
        time11.setText(monday[0].replace("|"," "));

        time12.setText(monday[1].replace("|"," "));

        String[] tuesday = dayString2.split("\\ - ");
        time21.setText(tuesday[0].replace("|"," "));

        time22.setText(tuesday[1].replace("|"," "));

        String[] wednessday =dayString3.split("\\ - ");
        time31.setText(wednessday[0].replace("|"," "));

        time32.setText(wednessday[1].replace("|"," "));

        String[] thursday =dayString4.split("\\ - ");
        time41.setText(thursday[0].replace("|"," "));

        time42.setText(thursday[1].replace("|"," "));

        String[] friday = dayString5.split("\\ - ");
        time51.setText(friday[0].replace("|"," "));

        time52.setText(friday[1].replace("|"," "));

        String[] saturday = dayString6.split("\\ - ");
        time61.setText(saturday[0].replace("|"," "));

        time62.setText(saturday[1].replace("|"," "));


        String[] sunday = dayString7.split("\\ - ");


        Log.e("s",sunday[0]);

        time71.setText(sunday[0].replace("|"," "));

        time72.setText(sunday[1].replace("|"," "));

    }

}
