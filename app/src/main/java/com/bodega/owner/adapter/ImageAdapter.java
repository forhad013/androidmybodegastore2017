package com.bodega.owner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bodega.owner.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by Nahid on 1/19/2016.
 */
public class ImageAdapter extends BaseAdapter {



    ArrayList<String>  ProductImage;
    int tag;

    private Context context;

    public ImageAdapter(Context context, ArrayList<String> ProductImage  ) {

        // TODO Auto-generated constructor stub
        this.context = context;

        this.ProductImage = ProductImage;


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return ProductImage.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;


        if (view == null) {

            view = LayoutInflater.from(context).inflate(
                    R.layout.grid_item, parent, false);
        }

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        ImageView image1 = (ImageView) view.findViewById(R.id.imageGrid);
        String url =ProductImage.get(position);
        ImageLoader.getInstance().displayImage(url, image1, defaultOptions);
//        ImageLoader.getInstance().displayImage(officePicString, storePic1, defaultOptions);
      //  ImageLoader.getInstance().displayImage(legalPicString, legalPic, defaultOptions);


        return view;
    }

}