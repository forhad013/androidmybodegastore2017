package com.bodega.owner.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class JsonModelBasicInventoryUpdate {


    public String inventory_item_name;

    public String inventory_price;
    public String inventory_status;
    public  String user_email;


    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
          //  obj.put("inventory_id", inventory_id);

            obj.put("inventory_item_name", inventory_item_name);
            obj.put("inventory_price", inventory_price);
            obj.put("inventory_status", inventory_status);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }




}
