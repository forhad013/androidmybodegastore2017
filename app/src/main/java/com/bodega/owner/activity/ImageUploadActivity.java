package com.bodega.owner.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.MyDatabase;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.SharePref;
import com.luminous.pick.CustomGallery;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.sw926.imagefileselector.ImageCropper;
import com.sw926.imagefileselector.ImageFileSelector;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ImageUploadActivity extends AppCompatActivity {


    ImageCropper mImageCropper;

    ImageFileSelector mImageFileSelector;

    String userID,imageLink;

    Button exit,done,crop;

    ImageView imageView;

    TextView gallery,camera;

    String currentImageFile;

    ImageLoader imageLoader;

    MyDatabase db;

    ImageButton backBtn;

    JSONObject json;
    JSONParser jsonParser;

    String encodedFileString;

    int success;

    SharePref sharePref;
    String errorString;

    SweetAlertDialog doneDialog,progressSweetAlertDialog;

    String currentImageCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);



        db = new MyDatabase(getApplicationContext());


        jsonParser = new JSONParser();


        backBtn = (ImageButton) findViewById(R.id.btnPrev);
          done = (Button)findViewById(R.id.done);
       crop = (Button)findViewById(R.id.crop);

         exit = (Button) findViewById(R.id.cancel);
         gallery = (TextView)findViewById(R.id.gallery);

         camera = (TextView) findViewById(R.id.camera);

          imageView = (ImageView) findViewById(R.id.image);


        sharePref = new SharePref(getApplicationContext());


        userID = sharePref.getshareprefdatastring(SharePref.USERID);



        currentImageCategory =  getIntent().getStringExtra("token");

        if(currentImageCategory.equals("additionalPic")){
            imageLink = "";
        }else{
            imageLink = getIntent().getStringExtra("link");
        }

        // save = (Button) dialog.findViewById(R.id.submit);

        progressSweetAlertDialog = new SweetAlertDialog(ImageUploadActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = new File(currentImageFile);

                mImageCropper.cropImage(file);

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              if(  imageView.getDrawable() != null){
                  encodedFileString= encodeImages(currentImageFile);

                  new AsyncTaskRunnerDetails().execute();
              }

            }
        });

        mImageFileSelector = new ImageFileSelector(this);

        mImageFileSelector.setCallback(new ImageFileSelector.Callback() {
            @Override
            public void onSuccess(final String file) {
                if (!TextUtils.isEmpty(file)) {


                    final Bitmap bitmap = BitmapFactory.decodeFile(file);
                    File imageFile = new File(file);



                    currentImageFile =   imageFile.getPath();

                    Log.e("file", currentImageFile);
                    imageView.setImageBitmap(bitmap);
                    imageView.setVisibility(View.VISIBLE);
                    crop.setVisibility(View.VISIBLE);


                } else {
                    Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError() {
                Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
            }
        });

        mImageCropper = new ImageCropper(this);

        mImageCropper.setCallback(new ImageCropper.ImageCropperCallback() {
            @Override
            public void onCropperCallback(ImageCropper.CropperResult result, File srcFile, File outFile) {
                currentImageFile = "";
                //  mBtnCrop.setVisibility(View.GONE);
                if (result == ImageCropper.CropperResult.success) {
                    currentImageFile = outFile.getPath();

                    final Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(outFile));
                    File imageFile = new File(String.valueOf(outFile));



                    currentImageFile =   imageFile.getPath();

                    imageView.setImageBitmap(bitmap);
                    crop.setVisibility(View.VISIBLE);

                } else if (result == ImageCropper.CropperResult.error_illegal_input_file) {
                    Toast.makeText(getApplicationContext(), "input file error", Toast.LENGTH_LONG).show();
                } else if (result == ImageCropper.CropperResult.error_illegal_out_file) {
                    Toast.makeText(getApplicationContext(), "output file error", Toast.LENGTH_LONG).show();
                }
            }
        });



        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });



        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("file", "Asdsd");


                mImageFileSelector.selectImage(ImageUploadActivity.this);


            }
        });


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageFileSelector.takePhoto(ImageUploadActivity.this);

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentImageFile="";

                finish();
            }
        });



    }




    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    //    mImageFileSelector.onSaveInstanceState(outState);
     //   mImageCropper.onSaveInstanceState(outState);
    }




    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
       // mImageFileSelector.onRestoreInstanceState(savedInstanceState);
      //  mImageCropper.onRestoreInstanceState(savedInstanceState);
    }
    @SuppressWarnings("NullableProblems")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImageFileSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mImageFileSelector.onActivityResult(requestCode, resultCode, data);
        mImageCropper.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
//            adapter.clear();
//
//            viewSwitcher.setDisplayedChild(1);
            String single_path = data.getStringExtra("single_path");
            //  imageLoader.displayImage("file://" + single_path, imgSinglePick);


            currentImageFile = single_path;


            // Uri uri = data.getData();

            Uri uri = Uri.parse("file://"+currentImageFile);

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);


                // Log.d(TAG, String.valueOf(bitmap));

//                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                //  imageView.setImageBitmap(bitmap);


            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            String[] all_path = data.getStringArrayExtra("all_path");

            ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();

            for (String string : all_path) {
                CustomGallery item = new CustomGallery();
                item.sdcardPath = string;

                dataT.add(item);
            }


        }
    }




    class AsyncTaskRunnerDetails extends AsyncTask<String, String, String> {

        int success = 5, mailcheck = 5;

        HttpResponse response;
        JSONObject response1;
        String responseBody;
        Boolean availableProduct, availablemail;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            {

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        Url.UPDATE);

                httpPost.setHeader("content-type", "application/json");
                httpPost.setHeader("Accept", "application/json");
                //  JSONObject jsonObject1 = new JSONObject();
                JSONObject jsonObject = new JSONObject();


                try {
                    jsonObject.accumulate("storeUpdateType", "storeImages");

                    jsonObject.accumulate("imageType",currentImageCategory);
                    jsonObject.accumulate("imageUrl", imageLink);
                    jsonObject.accumulate("imageData", encodedFileString);

                    jsonObject.accumulate("userId", userID);
                    jsonObject.accumulate("userType", "1");



                } catch (JSONException e) {
                    e.printStackTrace();
                }







                    Log.i("res",jsonObject.toString()+"");


                StringEntity entity = null;




                try {
                    entity = new StringEntity( jsonObject.toString());
                    entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    // Log.d("ch",entity+"");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                httpPost.setEntity(entity);

                try {
                    //  response = httpClient.execute(httpPost);
//                    resC++;
//                    Log.e("Value of response count: ", resC + "");
                    ResponseHandler<String> responseHandler=new BasicResponseHandler();
                    responseBody =httpClient.execute(httpPost, responseHandler);
                    response1=new JSONObject(responseBody);
                    Log.e("res", responseBody + "");



                    // invoiceNumber= response1.getString("orderno");



                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(String string) {

            progressSweetAlertDialog.dismiss();

            int success;
            String msg = "";
            try {
                success =  response1.getInt("success");

                msg = response1.getString("message");

                if(success==1){

                    try {

                        DiskCacheUtils.removeFromCache(imageLink, ImageLoader.getInstance().getDiskCache());

                        MemoryCacheUtils.removeFromCache(imageLink, ImageLoader.getInstance().getMemoryCache());
                    }catch (Exception e){

                    }

                    if(currentImageCategory.equals("additionalPic")) {
                        getData(response1);
                    }

                    doneDialog=  new SweetAlertDialog(ImageUploadActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("Success");
                    doneDialog.setContentText(msg);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
//                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//
//
//                            startActivity(intent);
                            doneDialog.dismiss();
                            finish();
                        }
                    });


                }else if(success ==0){

                   // getError(response1);



                    doneDialog=  new SweetAlertDialog(ImageUploadActivity.this,SweetAlertDialog.ERROR_TYPE);


                    doneDialog.setContentText("Something wrong!! Try again later!!");
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    });

                }



            }catch (JSONException e) {
                e.printStackTrace();
            }


         //   done.setEnabled(true);



        }



        @Override
        protected void onPreExecute() {

            progressSweetAlertDialog.show();
          //  done.setEnabled(false);

        }

    }


    public void getData(JSONObject json){

        String newline = System.getProperty("line.separator");

        try {
            JSONObject result = json.getJSONObject("results");


            JSONArray errors= result.getJSONArray("updateInfo");

            for (int i=0;i<errors.length();i++){

                JSONObject temp = errors.getJSONObject(i);


                String type = temp.getString("imageType");

                if(type.equals("additionalImage")){
                    String link = temp.getString("imageUrl");
                    db.saveImages(userID,"additional",link);
                    Log.e("lo",link);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public String encodeImages(String filePath){

        String encodedString="";

        BitmapFactory.Options options = null;
        options = new BitmapFactory.Options();
        options.inSampleSize = 3;

        Bitmap bitmap;

        bitmap = BitmapFactory.decodeFile(filePath, options);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // Must compress the Image to reduce image size to make
        // upload easy
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byte_arr = stream.toByteArray();
        // Encode Image to String
        encodedString = Base64.encodeToString(byte_arr,
                Base64.DEFAULT);


        return  encodedString;

    }
}
