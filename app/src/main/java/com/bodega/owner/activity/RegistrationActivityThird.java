package com.bodega.owner.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;
import com.sw926.imagefileselector.ImageCropper;
import com.sw926.imagefileselector.ImageFileSelector;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class RegistrationActivityThird extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;



    SharePref sharePref;

    ConnectionDetector cd;
    int success = 0;
    ImageButton secondNxtBtn;

    ImageButton nextPage,previousPage;
    TextView   legalInfo;
    Url url;
    JSONObject json;

    String msg;

    ImageButton backBtn;

    boolean isInternetOn=false;

    String storeLandLineString;

    ImageButton legalPermitButton;
    boolean deliveryOption0,deliveryOption1,deliveryOption2,deliveryOption3,deliveryOption4;

    String deliveryOption2Value,deliveryOption3Value,deliveryOption4Value;

    ImageFileSelector mImageFileSelector;
    ImageCropper mImageCropper;
    ArrayList timeList,ampmlist;

    ArrayList hours,additaionalImagePath;

    String personalImagePath,firstNameString,lastNameString;

    String commercialNameString,webNameString,phoneString,storeImageString;
    String userID,gcmCode,shopNameString,addressString,passwordString1,passwordString2,contactString,emailString,message;

    String legalNameString,legalCertificateImage="";
    JSONParser jsonParser;

    String single_path="";

    InputMethodManager imm;

    String callOption;

    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;

    private static final String LOG_TAG = "Google Places Autocomplete";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";

    private static final String OUT_JSON = "/json";
    LinearLayout lin;

    private static final String API_KEY =   "AIzaSyB_wVRhZ1qu9IVJO3JyS_fIPIfOkoLSjUA";


    TextView time11,time12,time21,time22,time31,time32,time41,time42,time51,time52,time61,time62,time71,time72,legalEmail;



    ArrayList <Integer> timeListInt;

    EditText legalName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_third);


        additaionalImagePath = new ArrayList();

        hours = new ArrayList();
        legalInfo = (TextView) findViewById(R.id.legalInfo);
        time11 = (TextView) findViewById(R.id.spinnerAM1);
        time12 = (TextView) findViewById(R.id.spinnerPM1);

        time21 = (TextView) findViewById(R.id.spinnerAM2);
        time22 = (TextView) findViewById(R.id.spinnerPM2);

        time31 = (TextView) findViewById(R.id.spinnerAM3);
        time32 = (TextView) findViewById(R.id.spinnerPM3);

        time41 = (TextView) findViewById(R.id.spinnerAM4);
        time42 = (TextView) findViewById(R.id.spinnerPM4);

        time51 = (TextView) findViewById(R.id.spinnerAM5);
        time52 = (TextView) findViewById(R.id.spinnerPM5);

        time61 = (TextView) findViewById(R.id.spinnerAM6);
        time62 = (TextView) findViewById(R.id.spinnerPM6);

        time71 = (TextView) findViewById(R.id.spinnerAM7);
        time72 = (TextView) findViewById(R.id.spinnerPM7);
        legalEmail= (TextView) findViewById(R.id.legalEmail);



        lin = (LinearLayout) findViewById(R.id.lin);

         getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        imm = (InputMethodManager)  getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(lin.getWindowToken(), 0);

        timeListInt = new ArrayList<>();

        ampmlist = new ArrayList();

        timeList = new ArrayList();

        ImageFileSelector.setDebug(true);
        timeList.add("00.00");
        timeList.add("01.00");
        timeList.add("02.00");
        timeList.add("03.00");
        timeList.add("04.00");
        timeList.add("05.00");
        timeList.add("06.00");
        timeList.add("07.00");
        timeList.add("08.00");
        timeList.add("09.00");
        timeList.add("10.00");
        timeList.add("11.00");
        timeList.add("12.00");






cd = new ConnectionDetector(getApplicationContext());


        callOption = getIntent().getStringExtra("callOption");
        firstNameString = getIntent().getStringExtra("firstName");
        lastNameString = getIntent().getStringExtra("lastName");
        emailString = getIntent().getStringExtra("email");
        passwordString1 = getIntent().getStringExtra("password");
        personalImagePath = getIntent().getStringExtra("personalPicPath");


        commercialNameString = getIntent().getStringExtra("commercialName");
        addressString = getIntent().getStringExtra("address");
        phoneString= getIntent().getStringExtra("phone");
        webNameString = getIntent().getStringExtra("webName");
        storeImageString = getIntent().getStringExtra("storeImage");

        storeLandLineString = getIntent().getStringExtra("landLine");

        legalCertificateImage = getIntent().getStringExtra("legalImage");

        legalNameString = getIntent().getStringExtra("legalName");
        timeListInt = getIntent().getIntegerArrayListExtra("hoursInt");

        hours = getIntent().getIntegerArrayListExtra("hours");

        deliveryOption2Value = getIntent().getStringExtra("deliveryOp2value");
        deliveryOption3Value = getIntent().getStringExtra("deliveryOp3value");
        deliveryOption4Value = getIntent().getStringExtra("deliveryOp4value");

        deliveryOption1 = getIntent().getBooleanExtra("deliveryOp1", false);

        deliveryOption0 = getIntent().getBooleanExtra("deliveryOp0", false);

        deliveryOption2 = getIntent().getBooleanExtra("deliveryOp2", false);

        deliveryOption3 = getIntent().getBooleanExtra("deliveryOp3", false);
        deliveryOption4 = getIntent().getBooleanExtra("deliveryOp4", false);

        additaionalImagePath = getIntent().getStringArrayListExtra("additionalPic");




        jsonParser = new JSONParser();
        url = new Url();
        sharePref = new SharePref(getApplicationContext());

        nextPage = (ImageButton) findViewById(R.id.btnNext);


        legalName = (EditText) findViewById(R.id.legalName);

        secondNxtBtn = (ImageButton) findViewById(R.id.btnNext2);



        legalPermitButton = (ImageButton) findViewById(R.id.legalPermitBtn);

        previousPage = (ImageButton) findViewById(R.id.btnPrev);

        legalName.setText(legalNameString);

        legalCertificateImage = legalCertificateImage+"";


        if (hours.size() != 0) {


            setTimeList();
        }


        legalEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    ;

                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"owner@mybodega.online"});

                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(RegistrationActivityThird.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });


        legalPermitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent i = new Intent(Action.ACTION_PICK);
//                startActivityForResult(i, 100);

            //    imagePickerDialoge();

                Intent intent = new Intent(RegistrationActivityThird.this,
                        ImageSelectActivity.class);

                intent.putExtra("token","legal");


                startActivity(intent);

            }
        });



        legalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"legal@mybodega.online"});

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(RegistrationActivityThird.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        secondNxtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isInternetOn = cd.isConnectingToInternet();

                if(isInternetOn) {

                    if (nullCheck()) {


                        hours.add(time11.getText().toString().replace(" ", "|") + " - " + time12.getText().toString().replace(" ", "|"));
                        hours.add(time21.getText().toString().replace(" ", "|") + " - " + time22.getText().toString().replace(" ", "|"));
                        hours.add(time31.getText().toString().replace(" ", "|") + " - " + time32.getText().toString().replace(" ", "|"));
                        hours.add(time41.getText().toString().replace(" ", "|") + " - " + time42.getText().toString().replace(" ", "|"));
                        hours.add(time51.getText().toString().replace(" ", "|") + " - " + time52.getText().toString().replace(" ", "|"));
                        hours.add(time61.getText().toString().replace(" ", "|") + " - " + time62.getText().toString().replace(" ", "|"));
                        hours.add(time71.getText().toString().replace(" ", "|") + " - " + time72.getText().toString().replace(" ", "|"));


                        Intent intent = new Intent(getApplicationContext(), RegistrationActivityFourth.class);

                        legalNameString = legalName.getText().toString();

                        intent.putExtra("firstName", firstNameString);
                        intent.putExtra("lastName", lastNameString);
                        intent.putExtra("email", emailString);
                        intent.putExtra("password", passwordString1);
                        intent.putExtra("personalPicPath", personalImagePath);
                        intent.putExtra("landLine", storeLandLineString);
                        intent.putExtra("commercialName", commercialNameString);
                        intent.putExtra("address", addressString);
                        intent.putExtra("webName", webNameString);
                        intent.putExtra("phone", phoneString);
                        intent.putExtra("storeImage", storeImageString);

                        intent.putExtra("legalName", legalNameString);
                        intent.putExtra("legalImage", legalCertificateImage);
                        intent.putStringArrayListExtra("hours", hours);
                        intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                        intent.putStringArrayListExtra("additionalPic", additaionalImagePath);


                        intent.putExtra("deliveryOp0", deliveryOption0);
                        intent.putExtra("deliveryOp1", deliveryOption1);
                        intent.putExtra("deliveryOp2", deliveryOption2);
                        intent.putExtra("deliveryOp3", deliveryOption3);
                        intent.putExtra("deliveryOp4", deliveryOption4);


                        intent.putExtra("deliveryOp2value", deliveryOption2Value);
                        intent.putExtra("deliveryOp3value", deliveryOption3Value);
                        intent.putExtra("deliveryOp4value", deliveryOption4Value);


                        startActivity(intent);

                        finish();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }

            }
        });


        time11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time11);
            }
        });
        time12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time12);
            }
        });
        time21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time21);
            }
        });
        time22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time22);
            }
        });
        time31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time31);
            }
        });
        time32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time32);
            }
        });
        time41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time41);
            }
        });

        time42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time42);
            }
        });

        time51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time51);
            }
        });

        time52.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time52);
            }
        });

         time61.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time61);
            }
        });
         time62.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time62);
            }
        });
         time71.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time71);
            }
        });
         time72.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePickerDialoge(time72);
            }
        });


        secondNxtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetOn = cd.isConnectingToInternet();

                if(isInternetOn) {
                    if (nullCheck()) {

                        hours.add(time11.getText().toString().replace(" ", "|") + " - " + time12.getText().toString().replace(" ", "|"));
                        hours.add(time21.getText().toString().replace(" ", "|") + " - " + time22.getText().toString().replace(" ", "|"));
                        hours.add(time31.getText().toString().replace(" ", "|") + " - " + time32.getText().toString().replace(" ", "|"));
                        hours.add(time41.getText().toString().replace(" ", "|") + " - " + time42.getText().toString().replace(" ", "|"));
                        hours.add(time51.getText().toString().replace(" ", "|") + " - " + time52.getText().toString().replace(" ", "|"));
                        hours.add(time61.getText().toString().replace(" ", "|") + " - " + time62.getText().toString().replace(" ", "|"));
                        hours.add(time71.getText().toString().replace(" ", "|") + " - " + time72.getText().toString().replace(" ", "|"));

                        Intent intent = new Intent(getApplicationContext(), RegistrationActivityFourth.class);

                        legalNameString = legalName.getText().toString();

                        intent.putExtra("firstName", firstNameString);
                        intent.putExtra("lastName", lastNameString);
                        intent.putExtra("email", emailString);
                        intent.putExtra("password", passwordString1);
                        intent.putExtra("personalPicPath", personalImagePath);
                        intent.putExtra("landLine", storeLandLineString);
                        intent.putExtra("commercialName", commercialNameString);
                        intent.putExtra("address", addressString);
                        intent.putExtra("webName", webNameString);
                        intent.putExtra("phone", phoneString);
                        intent.putExtra("storeImage", storeImageString);
                        intent.putExtra("callOption", callOption);


                        intent.putExtra("legalName", legalNameString);
                        intent.putExtra("legalImage", legalCertificateImage);
                        intent.putStringArrayListExtra("hours", hours);
                        intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                        intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

                        intent.putExtra("deliveryOp0", deliveryOption0);
                        intent.putExtra("deliveryOp1", deliveryOption1);
                        intent.putExtra("deliveryOp2", deliveryOption2);
                        intent.putExtra("deliveryOp3", deliveryOption3);
                        intent.putExtra("deliveryOp4", deliveryOption4);


                        intent.putExtra("deliveryOp2value", deliveryOption2Value);
                        intent.putExtra("deliveryOp3value", deliveryOption3Value);
                        intent.putExtra("deliveryOp4value", deliveryOption4Value);


                        startActivity(intent);

                        finish();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }


            }
        });
        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetOn = cd.isConnectingToInternet();

                if(isInternetOn) {
                    if (nullCheck()) {


                        hours.add(time11.getText().toString().replace(" ", "|") + " - " + time12.getText().toString().replace(" ", "|"));
                        hours.add(time21.getText().toString().replace(" ", "|") + " - " + time22.getText().toString().replace(" ", "|"));
                        hours.add(time31.getText().toString().replace(" ", "|") + " - " + time32.getText().toString().replace(" ", "|"));
                        hours.add(time41.getText().toString().replace(" ", "|") + " - " + time42.getText().toString().replace(" ", "|"));
                        hours.add(time51.getText().toString().replace(" ", "|") + " - " + time52.getText().toString().replace(" ", "|"));
                        hours.add(time61.getText().toString().replace(" ", "|") + " - " + time62.getText().toString().replace(" ", "|"));
                        hours.add(time71.getText().toString().replace(" ", "|") + " - " + time72.getText().toString().replace(" ", "|"));

                        Intent intent = new Intent(getApplicationContext(), RegistrationActivityFourth.class);

                        legalNameString = legalName.getText().toString();

                        intent.putExtra("firstName", firstNameString);
                        intent.putExtra("lastName", lastNameString);
                        intent.putExtra("email", emailString);
                        intent.putExtra("password", passwordString1);
                        intent.putExtra("personalPicPath", personalImagePath);
                        intent.putExtra("landLine", storeLandLineString);
                        intent.putExtra("commercialName", commercialNameString);
                        intent.putExtra("address", addressString);
                        intent.putExtra("webName", webNameString);
                        intent.putExtra("phone", phoneString);
                        intent.putExtra("storeImage", storeImageString);
                        intent.putExtra("callOption", callOption);


                        intent.putExtra("legalName", legalNameString);
                        intent.putExtra("legalImage", legalCertificateImage);
                        intent.putStringArrayListExtra("hours", hours);
                        intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                        intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

                        intent.putExtra("deliveryOp0", deliveryOption0);
                        intent.putExtra("deliveryOp1", deliveryOption1);
                        intent.putExtra("deliveryOp2", deliveryOption2);
                        intent.putExtra("deliveryOp3", deliveryOption3);
                        intent.putExtra("deliveryOp4", deliveryOption4);


                        intent.putExtra("deliveryOp2value", deliveryOption2Value);
                        intent.putExtra("deliveryOp3value", deliveryOption3Value);
                        intent.putExtra("deliveryOp4value", deliveryOption4Value);


                        startActivity(intent);

                        finish();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }

            }
        });


        previousPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                legalNameString = legalName.getText().toString();



                hours.add(time11.getText().toString().replace(" ","|")  + " - " + time12.getText().toString().replace(" ","|"));
                hours.add(time21.getText().toString().replace(" ","|") +   " - " + time22.getText().toString().replace(" ","|"));
                hours.add(time31.getText().toString().replace(" ","|") +  " - " + time32.getText().toString().replace(" ","|"));
                hours.add(time41.getText().toString().replace(" ","|") +   " - " + time42.getText().toString().replace(" ","|"));
                hours.add(time51.getText().toString().replace(" ","|") +   " - " +time52.getText().toString().replace(" ","|"));
                hours.add(time61.getText().toString().replace(" ","|") +   " - " +time62.getText().toString().replace(" ","|"));
                hours.add(time71.getText().toString().replace(" ","|") +   " - " +time72.getText().toString().replace(" ","|"));





               // Log.e("hours", hours + "");

                Intent intent = new Intent(getApplicationContext(), RegistrationActivitySecond.class);


                intent.putExtra("firstName", firstNameString);
                intent.putExtra("lastName", lastNameString);
                intent.putExtra("email", emailString);
                intent.putExtra("password", passwordString1);
                intent.putExtra("personalPicPath", personalImagePath);


                intent.putExtra("landLine", storeLandLineString);
                intent.putExtra("commercialName", commercialNameString);
                intent.putExtra("address", addressString);
                intent.putExtra("webName", webNameString);
                intent.putExtra("phone", phoneString);
                intent.putExtra("storeImage", storeImageString);
                intent.putExtra("callOption", callOption);



                intent.putExtra("legalName", legalNameString);
                intent.putExtra("legalImage", legalCertificateImage);
                intent.putStringArrayListExtra("hours", hours);
                intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                intent.putStringArrayListExtra("additionalPic", additaionalImagePath);


                intent.putExtra("deliveryOp0", deliveryOption0);
                intent.putExtra("deliveryOp1", deliveryOption1);
                intent.putExtra("deliveryOp2", deliveryOption2);
                intent.putExtra("deliveryOp3",deliveryOption3);
                intent.putExtra("deliveryOp4",deliveryOption4);


                intent.putExtra("deliveryOp2value",deliveryOption2Value);
                intent.putExtra("deliveryOp3value", deliveryOption3Value);
                intent.putExtra("deliveryOp4value", deliveryOption4Value);
                startActivity(intent);

                finish();

            }
        });



    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        mImageFileSelector.onActivityResult(requestCode, resultCode, data);
//        mImageCropper.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
////            adapter.clear();
////
////            viewSwitcher.setDisplayedChild(1);
//              single_path = data.getStringExtra("single_path");
//            //  imageLoader.displayImage("file://" + single_path, imgSinglePick);
//
//
//            legalCertificateImage = single_path;
//
//         //   Log.e("pic", legalCertificateImage);
//            // Uri uri = data.getData();
//
//
//
//
//        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
//            String[] all_path = data.getStringArrayExtra("all_path");
//
//            ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();
//
//            for (String string : all_path) {
//                CustomGallery item = new CustomGallery();
//                item.sdcardPath = string;
//
//                dataT.add(item);
//            }
//
////            viewSwitcher.setDisplayedChild(0);
////            adapter.addAll(dataT);
//        }
//    }

    @Override
    protected void onResume() {

        legalCertificateImage = sharePref.getshareprefdatastring(SharePref.LEGAL_IMAGE_PATH);

        if(!legalCertificateImage.equals("")){
            Uri uri = Uri.parse("file://" + legalCertificateImage);

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                legalPermitButton.setBackground(null);
                legalPermitButton.setImageBitmap(bitmap);

                // Log.d(TAG, String.valueOf(bitmap));

//                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                //  imageView.setImageBitmap(bitmap);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }




        super.onResume();
    }
    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }
    public boolean nullCheck() {
        boolean flag = false;





        Log.e("asd",legalCertificateImage.isEmpty()+"");
        Log.e("asd",legalCertificateImage);
       // legalCertificateImage.isEmpty();

        if (!legalName.getText().toString().trim().equalsIgnoreCase("")) {

                if(time11.getText().toString().length()!=0
                        ) {


                    return true;
                }else{
                    msg = "Please select appropriate time of store!";
                }



        } else {

            msg = "Please Enter Legal Name Of Your Store!";
        }


        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }










    public void timePickerDialoge(final TextView tview){
        final Dialog dialog = new Dialog(this);

        final String[] timeString = {""};
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.picker_dialog);
        // Set dialog title
        //dialog.setTitle("Category Add");

        dialog.show();

        Button done = (Button) dialog.findViewById(R.id.done);


        final WheelView wheelView = (WheelView) dialog.findViewById(R.id.wheelview);

        final WheelView amWheelView = (WheelView) dialog.findViewById(R.id.am_wheelview);
        ArrayList timeList = new ArrayList();

        timeList.add("00.00");

        timeList.add("00.30");
        timeList.add("01.00");
        timeList.add("01.30");
        timeList.add("02.00");
        timeList.add("02.30");
        timeList.add("03.00");
        timeList.add("03.30");
        timeList.add("04.00");
        timeList.add("04.30");
        timeList.add("05.00");
        timeList.add("05.30");
        timeList.add("06.00");
        timeList.add("06.30");
        timeList.add("07.00");
        timeList.add("07.30");
        timeList.add("08.00");
        timeList.add("08.30");
        timeList.add("09.00");
        timeList.add("09.30");
        timeList.add("10.00");
        timeList.add("10.30");
        timeList.add("11.00");
        timeList.add("11.30");
        timeList.add("12.00");



        ArrayList ampmList = new ArrayList();

        ampmList.add("am");
        ampmList.add("pm");


        wheelView.setWheelAdapter(new ArrayWheelAdapter(this));
        wheelView.setSkin(WheelView.Skin.Holo);
        wheelView.setWheelData(timeList);

        amWheelView.setWheelAdapter(new ArrayWheelAdapter(this));
        amWheelView.setSkin(WheelView.Skin.Holo);
        amWheelView.setWheelData(ampmList);
        wheelView.setSelection(10);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                timeString[0] = wheelView.getSelectionItem().toString();
                timeString[0] = timeString[0] +" "+ amWheelView.getSelectionItem().toString();
                Log.e("time",timeString[0]);


                tview.setText(timeString[0]);
                dialog.dismiss();



            }
        });


        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                timeString[0] = wheelView.getSelectionItem().toString();
                timeString[0] = timeString[0] +" "+ amWheelView.getSelectionItem().toString();
                Log.e("time",timeString[0]);


                tview.setText(timeString[0]);
            }
        });

    }

    @Override
    public void onBackPressed() {

        legalNameString = legalName.getText().toString();



        hours.add(time11.getText().toString().replace(" ","|")  + " - " + time12.getText().toString().replace(" ","|"));
        hours.add(time21.getText().toString().replace(" ","|") +   " - " + time22.getText().toString().replace(" ","|"));
        hours.add(time31.getText().toString().replace(" ","|") +  " - " + time32.getText().toString().replace(" ","|"));
        hours.add(time41.getText().toString().replace(" ","|") +   " - " + time42.getText().toString().replace(" ","|"));
        hours.add(time51.getText().toString().replace(" ","|") +   " - " +time52.getText().toString().replace(" ","|"));
        hours.add(time61.getText().toString().replace(" ","|") +   " - " +time62.getText().toString().replace(" ","|"));
        hours.add(time71.getText().toString().replace(" ","|") +   " - " +time72.getText().toString().replace(" ","|"));





        // Log.e("hours", hours + "");

        Intent intent = new Intent(getApplicationContext(), RegistrationActivitySecond.class);


        intent.putExtra("firstName", firstNameString);
        intent.putExtra("lastName", lastNameString);
        intent.putExtra("email", emailString);
        intent.putExtra("password", passwordString1);
        intent.putExtra("personalPicPath", personalImagePath);


        intent.putExtra("landLine", storeLandLineString);
        intent.putExtra("commercialName", commercialNameString);
        intent.putExtra("address", addressString);
        intent.putExtra("webName", webNameString);
        intent.putExtra("phone", phoneString);
        intent.putExtra("storeImage", storeImageString);
        intent.putExtra("callOption", callOption);



        intent.putExtra("legalName", legalNameString);
        intent.putExtra("legalImage", legalCertificateImage);
        intent.putStringArrayListExtra("hours", hours);
        intent.putIntegerArrayListExtra("hoursInt", timeListInt);
        intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

        intent.putExtra("deliveryOp0", deliveryOption0);
        intent.putExtra("deliveryOp1", deliveryOption1);
        intent.putExtra("deliveryOp2", deliveryOption2);
        intent.putExtra("deliveryOp3",deliveryOption3);
        intent.putExtra("deliveryOp4",deliveryOption4);


        intent.putExtra("deliveryOp2value",deliveryOption2Value);
        intent.putExtra("deliveryOp3value", deliveryOption3Value);
        intent.putExtra("deliveryOp4value", deliveryOption4Value);
        startActivity(intent);

        finish();


        super.onBackPressed();
    }

//    @Override
//    public void setContentView(View view)
//    {
//        super.setContentView(view);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
//    }

    public void setTimeList(){




        String[] sunday = hours.get(0).toString().split("\\ - ");


        Log.e("s",sunday[0]);

        time11.setText(sunday[0].replace("|"," "));

        time12.setText(sunday[1].replace("|"," "));


        String[] saturday = hours.get(6).toString().split("\\ - ");
        time71.setText(saturday[0].replace("|"," "));

        time72.setText(saturday[1].replace("|"," "));


        String[] monday = hours.get(1).toString().split("\\ - ");
        time21.setText(monday[0].replace("|"," "));

        time22.setText(monday[1].replace("|"," "));

        String[] tuesday = hours.get(2).toString().split("\\ - ");
        time31.setText(tuesday[0].replace("|"," "));

        time32.setText(tuesday[1].replace("|"," "));

        String[] wednessday = hours.get(3).toString().split("\\ - ");
        time41.setText(wednessday[0].replace("|"," "));

        time42.setText(wednessday[1].replace("|"," "));

        String[] thursday = hours.get(4).toString().split("\\ - ");
        time51.setText(thursday[0].replace("|"," "));

        time52.setText(thursday[1].replace("|"," "));

        String[] friday = hours.get(5).toString().split("\\ - ");
        time61.setText(friday[0].replace("|"," "));

        time62.setText(friday[1].replace("|"," "));

    }


}
