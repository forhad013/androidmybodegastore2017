package com.bodega.owner.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SplashActivity extends AppCompatActivity {

    boolean isInternetOn;

   ConnectionDetector cd;

    JSONParser jsonParser;
    SharePref sharePref;

    boolean status;
    String tokenStirng;

    SweetAlertDialog doneDialog;

    protected boolean _active = true;
    protected int _splashTime = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        jsonParser = new JSONParser();

        cd = new ConnectionDetector(getApplicationContext());

        sharePref = new SharePref(getApplicationContext());

        isInternetOn = cd.isConnectingToInternet();

        if(isInternetOn){

         // new AsyncTaskEdit().execute();
        }else{

            doneDialog=  new SweetAlertDialog(SplashActivity.this,SweetAlertDialog.ERROR_TYPE);
            doneDialog.setTitleText("Error");
            doneDialog.setContentText("Please turn on the internet connection and Restart the application");
            doneDialog.show();
            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {


                    finish();


                }
            });


        }




    }
//
//
//    class AsyncTaskEdit extends AsyncTask<String, String, String> {
//
//
//        @Override
//        protected void onPreExecute() {
//
//            try {
//
//
//
//                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
//            } catch (IndexOutOfBoundsException e) {
//
//            }
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//
//            List<NameValuePair> pair = new ArrayList<NameValuePair>();
//
//
//            JSONObject json = jsonParser.makeHttpRequest(Url.SOCKET_TOKEN, "GET", pair);
//
//            Log.e("json", Url.SOCKET_TOKEN + "");
//
//            try {
//
//
//
//                status = json.getBoolean("status");
//
//                if(!status){
//
//                    tokenStirng = json.getString("_token");
//                    Log.e("token",tokenStirng);
//                    sharePref.setshareprefdatastring(SharePref.SOCKET_TOKEN, tokenStirng);
//                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//
//
//            Thread splashTread = new Thread() {
//                @Override
//                public void run() {
//                    try {
//                        int waited = 0;
//                        while (_active && (waited < _splashTime)) {
//                            sleep(100);
//                            if (_active) {
//                                waited += 100;
//                            }
//                        }
//                    } catch (Exception e) {
//
//                    } finally {
//
//                        startActivity(new Intent(SplashActivity.this,
//                                LoginActivity.class));
//                        finish();
//                    }
//                };
//            };
//            splashTread.start();
//
//        }
//
//    }
    }

